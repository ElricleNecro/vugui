project "app"
	filename ".app"
	kind "ConsoleApp"
	language "C++"
	cppdialect "C++20"

	location "build/"
	targetdir "build/%{cfg.buildcfg}/bin"

	files { "src/%{prj.name}/**.cpp" }

	includedirs {
		"include/vugui",
		"include/%{prj.name}",
		"include/",
		"vendor/glm/",
		"vendor/imgui/imgui",
		"vendor/spdlog/include",
	}

	links {
		"ImGui",
		"vugui",
	}

	libdirs {
		"build/%{cfg.buildcfg}/lib"
	}

	warnings "Extra"
	flags "MultiProcessorCompile"

	filter "system:windows"
		systemversion "latest"
		defines { "VUGUI_PLATFORM_WINDOWS" }
		staticruntime "On"

	filter "system:linux"
		systemversion "latest"
		defines { "VUGUI_PLATFORM_LINUX" }
		pic "On"
		staticruntime "On"

	filter "configurations:Debug"
		defines { "DEBUG", "VUGUI_DEBUG", "VUGUI_PROFILE" }
		symbols "On"

	filter "configurations:Release"
		optimize "On"
		flags { "LinkTimeOptimization", "FatalWarnings" }
