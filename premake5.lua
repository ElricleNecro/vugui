workspace "VuGUI"
	architecture "x86_64"

	cppdialect "C++20"

	configurations { "Debug", "Release" }
		includedirs {
			"include/",
			"vendor/glm/",
			"vendor/imgui/imgui",
		}

		warnings "Extra"

		flags "MultiProcessorCompile"

include "vugui.lua"
include "example.lua"
