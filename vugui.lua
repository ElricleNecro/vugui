group "Dependencies"
	include "vendor/imgui"
group ""

project "vugui"
	filename ".vugui"
	kind "SharedLib"
	language "C++"
	cppdialect "C++20"

	location "build/"
	targetdir "build/%{cfg.buildcfg}/lib"

	files {
		"src/%{prj.name}/**.cpp",
	}

	includedirs {
		"include/%{prj.name}",
		"include/",
		"vendor/glm/",
		"vendor/imgui/imgui",
		"vendor/spdlog/include"
	}

	links {
		"ImGui",
		"vulkan",
		"glfw",
	}

	warnings "Extra"
	flags "MultiProcessorCompile"

	filter "system:windows"
		systemversion "latest"
		defines { "VUGUI_PLATFORM_WINDOWS" }
		staticruntime "off"
		pchsource "src/%{prj.name}/spch.cpp"

		files {
			"src/%{prj.name}/platform/windows/**.cpp",
		}

	filter "system:linux"
		systemversion "latest"
		defines { "VUGUI_PLATFORM_LINUX" }
		pic "On"
		staticruntime "off"

		files {
			"src/%{prj.name}/platform/linux/**.cpp",
		}

	filter "configurations:Debug"
		defines { "DEBUG", "VUGUI_DEBUG", "VUGUI_PROFILE" }
		runtime "Debug"
		symbols "On"

	filter "configurations:Release"
		flags { "LinkTimeOptimization", "FatalWarnings" }
		optimize "On"
		symbols "On"
		runtime "Release"

	filter "configurations:Dist"
		flags { "LinkTimeOptimization", "FatalWarnings" }
		optimize "Full"
		symbols "Off"
		runtime "Release"
