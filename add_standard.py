# coding: utf-8
from json import loads, dumps


if "__main__" == __name__:
    with open("compile_commands.json", "r") as inf:
        data = loads(inf.read())

    for file in data:
        if (
            "src/Editor" not in file["file"]
            and "src/Sandbox" not in file["file"]
            and "src/Solace" not in file["file"]
        ):
            continue
        file["command"] = file["command"].replace("cc", "cc -std=c++20")

    with open("compile_commands.json", "w") as outf:
        outf.write(dumps(data))
