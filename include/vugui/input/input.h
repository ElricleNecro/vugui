#pragma once

#include "vugui/input/keycodes.h"
#include <glm/glm.hpp>

namespace vugui {
	namespace input {
		bool is_key_down(KeyCode key);
		bool is_mouse_down(MouseButton button);

		glm::vec2 get_mouse_position(void);

		void set_cursor_mode(CursorMode mode);
	} // namespace input
} // namespace vugui
