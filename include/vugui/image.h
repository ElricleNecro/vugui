#pragma once

#include <string>
#include <string_view>
#include <vulkan/vulkan.h>

namespace vugui {
	class Image {
		public:
			enum class Format {
				None = 0,
				RGBA,
				RGBA32F,
			};

			enum class TextureSampling {
				NEAREST = VK_FILTER_NEAREST,
				LINEAR = VK_FILTER_LINEAR,
			};

			Image(const std::string_view &path, const TextureSampling sampling = TextureSampling::LINEAR);
			Image(const uint32_t width, const uint32_t height, const Format format, const TextureSampling sampling = TextureSampling::LINEAR, const void *data = nullptr);
			~Image(void);

			void set_data(const void *data);

			inline const uint32_t& width(void) const { return this->_width; };
			inline const uint32_t& height(void) const { return this->_height; };

			inline const VkDescriptorSet& descriptor_set(void) const { return this->_descriptor_set; };

			void resize(const uint32_t width, const uint32_t height);

		private:
			void allocate(const uint64_t size);
			void release(void);

			uint32_t _width, _height;
			Format _format;
			TextureSampling _sampling;

			// Vulkan informations:
			VkImage _image = nullptr;
			VkImageView _image_view = nullptr;
			VkDeviceMemory _mem = nullptr;
			VkSampler _sampler = nullptr;

			VkBuffer _staging_buffer = nullptr;
			VkDeviceMemory _staging_buffer_memory = nullptr;

			VkDescriptorSet _descriptor_set = nullptr;

			size_t alignment = 0;
			std::string file_path;
	};
} // namespace vugui
