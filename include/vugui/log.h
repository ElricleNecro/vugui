#pragma once

#include <memory>
#include "spdlog/spdlog.h"

namespace vugui {
	namespace log {
		class Log {
		public:
			static void init(void);

			inline static std::shared_ptr<spdlog::logger> &get_core_logger(void) {
				return Log::core;
			}

			inline static std::shared_ptr<spdlog::logger> &get_client_logger(void) {
				return Log::application;
			}

		private:
			static std::shared_ptr<spdlog::logger> core;
			static std::shared_ptr<spdlog::logger> application;
		};
	} // namespace log
} // namespace vugui

#define VUGUI_LOG_CORE_FATAL(...)    ::vugui::log::Log::get_core_logger()->fatal(__VA_ARGS__)
#define VUGUI_LOG_CORE_CRITICAL(...) ::vugui::log::Log::get_core_logger()->critical(__VA_ARGS__)
#define VUGUI_LOG_CORE_ERROR(...)    ::vugui::log::Log::get_core_logger()->error(__VA_ARGS__)
#define VUGUI_LOG_CORE_WARN(...)     ::vugui::log::Log::get_core_logger()->warn(__VA_ARGS__)
#define VUGUI_LOG_CORE_INFO(...)     ::vugui::log::Log::get_core_logger()->info(__VA_ARGS__)
#define VUGUI_LOG_CORE_TRACE(...)    ::vugui::log::Log::get_core_logger()->trace(__VA_ARGS__)
#define VUGUI_LOG_CORE_DEBUG(...)    ::vugui::log::Log::get_core_logger()->debug(__VA_ARGS__)

#define VUGUI_LOG_FATAL(...)    ::vugui::log::Log::get_client_logger()->fatal(__VA_ARGS__)
#define VUGUI_LOG_CRITICAL(...) ::vugui::log::Log::get_client_logger()->critical(__VA_ARGS__)
#define VUGUI_LOG_ERROR(...)    ::vugui::log::Log::get_client_logger()->error(__VA_ARGS__)
#define VUGUI_LOG_WARN(...)     ::vugui::log::Log::get_client_logger()->warn(__VA_ARGS__)
#define VUGUI_LOG_INFO(...)     ::vugui::log::Log::get_client_logger()->info(__VA_ARGS__)
#define VUGUI_LOG_TRACE(...)    ::vugui::log::Log::get_client_logger()->trace(__VA_ARGS__)
#define VUGUI_LOG_DEBUG(...)    ::vugui::log::Log::get_client_logger()->debug(__VA_ARGS__)
