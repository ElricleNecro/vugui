#pragma once

#include <functional>
#include <memory>
#include <string>
#include <vector>

#include <vulkan/vulkan.h>

#include "imgui.h"
#include "backends/imgui_impl_vulkan.h"

#include "layer.h"

struct GLFWwindow;

namespace vugui {
	void check_vk_result(VkResult err);

	struct MainApplicationSpecification {
		std::string name = "VuGUI application";

		uint32_t width = 1920;
		uint32_t height = 1080;
	};

	class MainApplication {
		public:
			MainApplication(const MainApplicationSpecification &spec = MainApplicationSpecification());
			~MainApplication(void);

			void run(void);

			inline void close(void) {
				this->running = false;
			};

			inline void set_menubar_callback(const std::function<void()> &callback) {
				this->menubar_callback = callback;
			}

			inline void push(const std::shared_ptr<Layer> &layer) {
				this->layer_stack.emplace_back(layer);
				layer->on_attach();
			}

			template<typename T>
			inline void push(void) {
				static_assert(std::is_base_of<Layer, T>::value, "Only class based on Layer can be pushed.");
				auto layer = std::make_shared<T>();
				this->layer_stack.emplace_back(layer);
				layer->on_attach();
			}

			float get_time(void);

			static MainApplication* get_application_instance(void) {
				return MainApplication::instance;
			};

			inline GLFWwindow* get_window_handle(void) const {
				return MainApplication::instance->window_handle;
			}

			inline VkInstance get_instance(void) const {
				return this->vk_instance;
			};
			inline VkPhysicalDevice get_physical_device(void) const {
				return this->vk_physical_device;
			};
			inline VkDevice get_device(void) const {
				return this->vk_device;
			};

			VkCommandBuffer get_command_buffer(bool begin);
			void flush_command_buffer(VkCommandBuffer buffer);

			inline void submit_resource_free(std::function<void()> &&func) {
				this->vk_resource_free_queue[this->vk_current_frame_index].emplace_back(func);
			};

		private:
			void setup_vulkan(const char **extensions, uint32_t extensions_count);
			void setup_vulkan_window(ImGui_ImplVulkanH_Window* wd, VkSurfaceKHR surface, int width, int height);

			void render_frame(ImDrawData *draw_data);
			void present_frame(void);

		private:
			MainApplicationSpecification spec;
			GLFWwindow *window_handle = nullptr;

			bool running = false;

			float time_step = 1.f;
			float frame_time = 0.f;
			float last_frame_time = 0.f;

			std::vector<std::shared_ptr<Layer>> layer_stack;
			std::function<void()> menubar_callback;

			// Vulkan stuff:
			VkAllocationCallbacks *vk_allocator = NULL;
			VkInstance vk_instance = VK_NULL_HANDLE;
			VkPhysicalDevice vk_physical_device = VK_NULL_HANDLE;
			VkDevice vk_device = VK_NULL_HANDLE;
			VkQueue vk_queue = VK_NULL_HANDLE;
			VkPipelineCache vk_pipeline_cache = VK_NULL_HANDLE;
			VkDescriptorPool vk_descriptor_pool = VK_NULL_HANDLE;

			uint32_t vk_queue_family = (uint32_t)-1;
			uint32_t vk_current_frame_index = 0;
			bool vk_swap_chain_rebuild = false;
			std::vector<std::vector<VkCommandBuffer>> vk_allocated_command_buffers;
			std::vector<std::vector<std::function<void()>>> vk_resource_free_queue;

			ImGui_ImplVulkanH_Window main_window_data;

			static MainApplication *instance;
	};

	MainApplication* create_application(int argc, char **argv);
} // namespace vugui
