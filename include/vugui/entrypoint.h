#pragma once

#include "application.h"
#include "log.h"

int main(int argc, char **argv) {
	vugui::log::Log::init();

	vugui::MainApplication *app = vugui::create_application(argc, argv);

	app->run();
	delete app;

	return 0;
}
