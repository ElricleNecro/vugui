
#pragma once

#include <random>

#include "glm/glm.hpp"

namespace vugui {
	class RandomNumber {
		public:
			static inline void init(void) {
				random_engine.seed(std::random_device()());
			}

			static inline uint32_t as_uint32_t(void) {
				return distribution(random_engine);
			}

			static inline uint32_t as_uint32_t(const uint32_t min, const uint32_t max) {
				return min + (distribution(random_engine) % (max - min + 1));
			}

			static inline uint32_t range(const uint32_t min, const uint32_t max) {
				return min + distribution(random_engine) % (max - min + 1);
			}

			static inline float as_float(void) {
				return (float)distribution(random_engine) / (float)distribution.max();
			}

			static inline float range(const float min, const float max) {
				return as_float() * (max - min) + min;
			}

			static inline glm::vec3 as_vec3(void) {
				return glm::vec3(
					as_float(),
					as_float(),
					as_float()
				);
			}

			static inline glm::vec3 as_vec3(const float min, const float max) {
				return glm::vec3(
					as_float() * (max - min) + min,
					as_float() * (max - min) + min,
					as_float() * (max - min) + min
				);
			}

			static inline glm::vec3 in_unit_sphere(void) {
				return glm::normalize(as_vec3(-1.f, 1.f));
			}

		private:
			static thread_local std::mt19937 random_engine;
			static std::uniform_int_distribution<std::mt19937::result_type> distribution;
	};
} // namespace vugui
