#pragma once

#include <iostream>
#include <string>
#include <chrono>

namespace vugui {
	class Timer {
		public:
			Timer(void) {
				this->reset();
			}

			void reset(void) {
				this->start = std::chrono::high_resolution_clock::now();
			}

			operator float(void) const {
				return std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::high_resolution_clock::now() - this->start).count() * 1e-9f;
			}

			float in_millis(void) const {
				return *this * 1000.f;
			}

		private:
			std::chrono::time_point<std::chrono::high_resolution_clock> start;
	};

	class ScopedTimer : private Timer {
		public:
			ScopedTimer(const std::string &name) : Timer(), name(name) {}
			~ScopedTimer(void) {
				std::cout << "[TIMER " <<  this->name << "] " << this->in_millis() << "ms" << std::endl;
			}

		private:
			std::string name;
	};
} // namespace vugui
