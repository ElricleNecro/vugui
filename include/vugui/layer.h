#pragma once

namespace vugui {
	class Layer {
		public:
			virtual ~Layer(void) = default;

			virtual void on_attach(void) {}
			virtual void on_detach(void) {}
			virtual void on_update([[maybe_unused]] float time_step) {}
			virtual void on_gui_rendering(void) {}
	};
} // namespace vugui
