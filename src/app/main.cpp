#include "vugui/application.h"
#include "vugui/entrypoint.h"
#include "vugui/layer.h"

#include "imgui.h"

class ExampleLayer : public vugui::Layer {
	public:
		virtual void on_gui_rendering(void) override {
			ImGui::Begin("Test Window");
			ImGui::Button("Pushy");
			ImGui::End();

			ImGui::ShowDemoWindow();
		}
};

vugui::MainApplication* vugui::create_application([[maybe_unused]]int argc, [[maybe_unused]]char **argv) {
	vugui::MainApplicationSpecification spec;
	spec.name = "Example";
	vugui::MainApplication *app = new MainApplication(spec);

	app->push<ExampleLayer>();
	app->set_menubar_callback([app]() {
		if( ImGui::BeginMenu("File") ) {
			if( ImGui::MenuItem("Quit") )
				app->close();
			ImGui::EndMenu();
		}
	});

	return app;
}
