#include "vugui/image.h"

#include "vugui/application.h"

#include "vugui/vendor/stb/stb_image.h"

namespace vugui {
	namespace utils {
		static inline uint32_t bytes_per_pixel(const Image::Format &format) {
			switch(format) {
				case Image::Format::RGBA:
					return 4;

				case Image::Format::RGBA32F:
					return 4;

				case Image::Format::None:
				default:
					return 0;
			}
		}

		static inline VkFormat to_vulkan(const Image::Format &format) {
			switch(format) {
				case Image::Format::RGBA:
					return VK_FORMAT_R8G8B8A8_UNORM;

				case Image::Format::RGBA32F:
					return VK_FORMAT_R32G32B32A32_SFLOAT;

				default:
					return (VkFormat)0;
			}
		}

		static uint32_t vulkan_memory_type(const VkMemoryPropertyFlags properties, const uint32_t type) {
			VkPhysicalDeviceMemoryProperties prop;
			vkGetPhysicalDeviceMemoryProperties(MainApplication::get_application_instance()->get_physical_device(), &prop);

			for(uint32_t idx=0; idx < prop.memoryTypeCount; idx++) {
				if( (type & (1 << idx)) && (prop.memoryTypes[idx].propertyFlags & properties) == properties )
					return idx;
			}

			return 0xffffffff;
		}
	} // namespace utils

	Image::Image(const std::string_view &path, const TextureSampling sampling) : _sampling(sampling), file_path(path) {
		int width, height, channels;
		uint8_t *data = nullptr;

		if( stbi_is_hdr(this->file_path.c_str()) ) {
			data = (uint8_t*)stbi_loadf(
				this->file_path.c_str(),
				&width,
				&height,
				&channels,
				4
			);
			this->_format = Format::RGBA32F;
		} else {
			data = stbi_load(
				this->file_path.c_str(),
				&width,
				&height,
				&channels,
				4
			);
			this->_format = Format::RGBA;
		}

		this->_width = width;
		this->_height = height;

		this->allocate(this->_width * this->_height * utils::bytes_per_pixel(this->_format));
		this->set_data(data);
		stbi_image_free(data);
	}

	Image::Image(const uint32_t width, const uint32_t height, const Format format, const TextureSampling sampling, const void *data) 
		: _width(width), _height(height), _format(format), _sampling(sampling) {
		this->allocate(this->_width * this->_height * utils::bytes_per_pixel(this->_format));
		if( data != nullptr )
			this->set_data(data);
	}

	Image::~Image(void) {
		this->release();
	}

	void Image::allocate([[maybe_unused]] const uint64_t size) {
		VkDevice device = MainApplication::get_application_instance()->get_device();
		VkFormat vulkan_format = utils::to_vulkan(this->_format);

		// Create the Image
		{
			VkImageCreateInfo info = {};
			info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
			info.imageType = VK_IMAGE_TYPE_2D;
			info.format = vulkan_format;
			info.extent.width = this->_width;
			info.extent.height = this->_height;
			info.extent.depth = 1;
			info.mipLevels = 1;
			info.arrayLayers = 1;
			info.samples = VK_SAMPLE_COUNT_1_BIT;
			info.tiling = VK_IMAGE_TILING_OPTIMAL;
			info.usage = VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;
			info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
			info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
			check_vk_result(
				vkCreateImage(device, &info, nullptr, &this->_image)
			);

			VkMemoryRequirements req;
			vkGetImageMemoryRequirements(device, this->_image, &req);

			VkMemoryAllocateInfo alloc_info = {};
			alloc_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
			alloc_info.allocationSize = req.size;
			alloc_info.memoryTypeIndex = utils::vulkan_memory_type(VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, req.memoryTypeBits);
			check_vk_result(
				vkAllocateMemory(device, &alloc_info, nullptr, &this->_mem)
			);
			check_vk_result(
				vkBindImageMemory(device, this->_image, this->_mem, 0)
			);
		}

		// Create the Image View:
		{
			VkImageViewCreateInfo info = {};
			info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
			info.image = this->_image;
			info.viewType = VK_IMAGE_VIEW_TYPE_2D;
			info.format = vulkan_format;
			info.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			info.subresourceRange.levelCount = 1;
			info.subresourceRange.layerCount = 1;
			check_vk_result(
				vkCreateImageView(device, &info, nullptr, &this->_image_view)
			);
		}

		// Create sampler that will handle texture sampling:
		{
			VkSamplerCreateInfo info = {};
			info.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
			info.magFilter = (VkFilter)this->_sampling;
			info.minFilter = (VkFilter)this->_sampling;
			info.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
			info.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
			info.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
			info.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
			info.minLod = -1000;
			info.maxLod = 1000;
			info.maxAnisotropy = 1.0f;
			check_vk_result(
				vkCreateSampler(device, &info, nullptr, &this->_sampler)
			);
		}

		this->_descriptor_set = (VkDescriptorSet)ImGui_ImplVulkan_AddTexture(
			this->_sampler,
			this->_image_view,
			VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
		);
	}

	void Image::release(void) {
		MainApplication::get_application_instance()->submit_resource_free(
			[sampler = this->_sampler, image_view = this->_image_view, image = this->_image, memory = this->_mem, staging_buffer = this->_staging_buffer, staging_buffer_memory = this->_staging_buffer_memory]() {
				VkDevice device = MainApplication::get_application_instance()->get_device();

				vkDestroySampler(device, sampler, nullptr);
				vkDestroyImageView(device, image_view, nullptr);
				vkDestroyImage(device, image, nullptr);
				vkFreeMemory(device, memory, nullptr);
				vkDestroyBuffer(device, staging_buffer, nullptr);
				vkFreeMemory(device, staging_buffer_memory, nullptr);
			}
		);

		this->_sampler = nullptr;
		this->_image_view = nullptr;
		this->_image = nullptr;
		this->_mem = nullptr;
		this->_staging_buffer = nullptr;
		this->_staging_buffer_memory = nullptr;
	}

	void Image::set_data(const void *data) {
		const VkDevice device = MainApplication::get_application_instance()->get_device();
		const size_t size = this->_width * this->_height * utils::bytes_per_pixel(this->_format);

		if( ! this->_staging_buffer ) {
			// Create the staging Buffer
			{
				VkBufferCreateInfo buffer_info = {};
				buffer_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
				buffer_info.size = size;
				buffer_info.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
				buffer_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
				check_vk_result(
					vkCreateBuffer(
						device,
						&buffer_info,
						nullptr,
						&this->_staging_buffer
					)
				);

				VkMemoryRequirements req;
				vkGetBufferMemoryRequirements(device, this->_staging_buffer, &req);
				this->alignment = req.size;

				VkMemoryAllocateInfo alloc_info = {};
				alloc_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
				alloc_info.allocationSize = req.size;
				alloc_info.memoryTypeIndex = utils::vulkan_memory_type(VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, req.memoryTypeBits);
				check_vk_result(
					vkAllocateMemory(
						device,
						&alloc_info,
						nullptr,
						&this->_staging_buffer_memory
					)
				);
				check_vk_result(
					vkBindBufferMemory(
						device,
						this->_staging_buffer,
						this->_staging_buffer_memory,
						0
					)
				);
			}
		}

		// Upload to Buffer
		{
			char* map = NULL;
			check_vk_result(
				vkMapMemory(
					device,
					this->_staging_buffer_memory,
					0,
					this->alignment,
					0,
					(void**)(&map)
				)
			);
			memcpy(map, data, size);

			VkMappedMemoryRange range[1] = {};
			range[0].sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
			range[0].memory = this->_staging_buffer_memory;
			range[0].size = this->alignment;
			check_vk_result(
				vkFlushMappedMemoryRanges(
					device,
					1,
					range
				)
			);
			vkUnmapMemory(device, this->_staging_buffer_memory);
		}

		// Copy to Image
		{
			VkCommandBuffer command_buffer = MainApplication::get_application_instance()->get_command_buffer(true);

			VkImageMemoryBarrier copy_barrier = {};
			copy_barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
			copy_barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
			copy_barrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
			copy_barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
			copy_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			copy_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			copy_barrier.image = this->_image;
			copy_barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			copy_barrier.subresourceRange.levelCount = 1;
			copy_barrier.subresourceRange.layerCount = 1;
			vkCmdPipelineBarrier(
				command_buffer,
				VK_PIPELINE_STAGE_HOST_BIT,
				VK_PIPELINE_STAGE_TRANSFER_BIT,
				0,
				0,
				NULL,
				0,
				NULL,
				1,
				&copy_barrier
			);

			VkBufferImageCopy region = {};
			region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			region.imageSubresource.layerCount = 1;
			region.imageExtent.width = this->_width;
			region.imageExtent.height = this->_height;
			region.imageExtent.depth = 1;
			vkCmdCopyBufferToImage(
				command_buffer,
				this->_staging_buffer,
				this->_image,
				VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
				1,
				&region
			);

			VkImageMemoryBarrier use_barrier = {};
			use_barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
			use_barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
			use_barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
			use_barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
			use_barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
			use_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			use_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			use_barrier.image = this->_image;
			use_barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			use_barrier.subresourceRange.levelCount = 1;
			use_barrier.subresourceRange.layerCount = 1;
			vkCmdPipelineBarrier(
				command_buffer,
				VK_PIPELINE_STAGE_TRANSFER_BIT,
				VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
				0,
				0,
				NULL,
				0,
				NULL,
				1,
				&use_barrier
			);

			MainApplication::get_application_instance()->flush_command_buffer(command_buffer);
		}
	}

	void Image::resize(const uint32_t width, const uint32_t height) {
		if( this->_image && this->_width == width && this->_height == height)
			return ;

		this->_width = width;
		this->_height = height;

		this->release();
		this->allocate(this->_width * this->_height * utils::bytes_per_pixel(this->_format));
	}
} // namespace vugui
