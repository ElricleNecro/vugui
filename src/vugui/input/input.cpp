#include "vugui/input/input.h"
#include "vugui/application.h"

#include <GLFW/glfw3.h>

namespace vugui {
	namespace input {
		bool is_key_down(KeyCode key) {
			GLFWwindow *handle = MainApplication::get_application_instance()->get_window_handle();

			int state = glfwGetKey(handle, (int)key);
			return state == GLFW_PRESS || state == GLFW_REPEAT;
		}

		bool is_mouse_down(MouseButton button) {
			GLFWwindow *handle = MainApplication::get_application_instance()->get_window_handle();

			int state = glfwGetMouseButton(handle, (int)button);
			return state == GLFW_PRESS;
		}

		glm::vec2 get_mouse_position(void) {
			GLFWwindow *handle = MainApplication::get_application_instance()->get_window_handle();

			double x, y;
			glfwGetCursorPos(handle, &x, &y);
			return { (float)x, (float)y };
		}

		void set_cursor_mode(CursorMode mode) {
			GLFWwindow *handle = MainApplication::get_application_instance()->get_window_handle();

			glfwSetInputMode(handle, GLFW_CURSOR, GLFW_CURSOR_NORMAL + (int)mode);
		}
	} // namespace input
} // namespace vugui
