#include "vugui/log.h"

#include "spdlog/cfg/env.h"
#include "spdlog/sinks/stdout_color_sinks.h"
#include "spdlog/sinks/basic_file_sink.h"

namespace vugui {
	namespace log {
		std::shared_ptr<spdlog::logger> Log::core;
		std::shared_ptr<spdlog::logger> Log::application;

		void Log::init(void) {
			spdlog::set_pattern("%^[%T] %l - %n: %v%$");
			spdlog::cfg::load_env_levels();

			std::vector<spdlog::sink_ptr> log_sinks;
			log_sinks.emplace_back(std::make_shared<spdlog::sinks::stdout_color_sink_mt>());
			// log_sinks.emplace_back(std::make_shared<spdlog::sinks::basic_file_sink_mt>("vugui.log", true));

			Log::core = std::make_shared<spdlog::logger>("vugui", begin(log_sinks), end(log_sinks));
			spdlog::register_logger(Log::core);
			Log::core->flush_on(spdlog::level::trace);
			// Log::core->set_level(spdlog::level::trace);

			Log::application = std::make_shared<spdlog::logger>("app", begin(log_sinks), end(log_sinks));
			spdlog::register_logger(Log::application);
			spdlog::set_default_logger(Log::application);
			Log::application->flush_on(spdlog::level::trace);
			// Log::application->set_level(spdlog::level::trace);
		}
	} // namespace log
} // namespace vugui
