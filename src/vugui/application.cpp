#include "vugui/application.h"

#include "backends/imgui_impl_glfw.h"
#include "backends/imgui_impl_vulkan.h"
#include "vugui/vendor/fonts/roboto.h"

#include <exception>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#define GLFW_INCLUDE_NONE
#define GLFW_INCLUDE_VULKAN
#define MIN_IMAGE_COUNT 2
#include <vulkan/vulkan.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

static void glfw_error_callback(int error, const char *descr) {
	fprintf(stderr, "GLFW error %d: %s\n", error, descr);
}

#if defined(_MSC_VER) && (_MSC_VER >= 1900) && !defined(IMGUI_DISABLE_WIN32_FUNCTIONS)
#pragma comment(lib, "legacy_stdio_definitions")
#endif

namespace vugui {
	void check_vk_result(VkResult err) {
		if( err == 0 )
			return;

		std::cerr << "[vulkan] error: VkResult = " << err << std::endl;

		if( err < 0 )
			throw std::runtime_error("Unable to recover from the error.");
	}

	MainApplication *MainApplication::instance = nullptr;

	MainApplication::MainApplication(const MainApplicationSpecification &spec) : spec(spec) {
		instance = this;

		glfwSetErrorCallback(glfw_error_callback);
		if( ! glfwInit() ) {
			throw std::runtime_error("Unable to initialise glfw.");
		}

		glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
		this->window_handle = glfwCreateWindow(
			this->spec.width,
			this->spec.height,
			this->spec.name.c_str(),
			NULL,
			NULL
		);

		if( ! glfwVulkanSupported() ) {
			throw std::runtime_error("Vulkan is not supported on your platform.");
		}

		uint32_t extensions_count = 0;
		const char **extensions = glfwGetRequiredInstanceExtensions(&extensions_count);

		this->setup_vulkan(extensions, extensions_count);

		VkSurfaceKHR surface;
		check_vk_result(
			glfwCreateWindowSurface(
				this->vk_instance,
				this->window_handle,
				this->vk_allocator,
				&surface
			)
		);

		int w, h;
		glfwGetFramebufferSize(this->window_handle, &w, &h);
		ImGui_ImplVulkanH_Window *wd = &this->main_window_data;
		this->setup_vulkan_window(wd, surface, w, h);

		this->vk_allocated_command_buffers.resize(wd->ImageCount);
		this->vk_resource_free_queue.resize(wd->ImageCount);

		IMGUI_CHECKVERSION();
		ImGui::CreateContext();
		ImGuiIO &io = ImGui::GetIO();

		io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
		io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;
		io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;

		ImGui::StyleColorsDark();

		ImGuiStyle& style = ImGui::GetStyle();
		if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
		{
			style.WindowRounding = 0.0f;
			style.Colors[ImGuiCol_WindowBg].w = 1.0f;
		}

		ImGui_ImplGlfw_InitForVulkan(this->window_handle, true);
		ImGui_ImplVulkan_InitInfo init_info = {};
		init_info.Instance = this->vk_instance;
		init_info.PhysicalDevice = this->vk_physical_device;
		init_info.Device = this->vk_device;
		init_info.QueueFamily = this->vk_queue_family;
		init_info.Queue = this->vk_queue;
		init_info.PipelineCache = this->vk_pipeline_cache;
		init_info.DescriptorPool = this->vk_descriptor_pool;
		init_info.Subpass = 0;
		init_info.MinImageCount = MIN_IMAGE_COUNT;
		init_info.ImageCount = wd->ImageCount;
		init_info.MSAASamples = VK_SAMPLE_COUNT_1_BIT;
		init_info.Allocator = this->vk_allocator;
		init_info.CheckVkResultFn = check_vk_result;
		ImGui_ImplVulkan_Init(&init_info, wd->RenderPass);

		ImFontConfig font_config;
		font_config.FontDataOwnedByAtlas = false;
		ImFont *roboto = io.Fonts->AddFontFromMemoryTTF((void*)font_roboto_regular, sizeof(font_roboto_regular), 20.f, &font_config);
		io.FontDefault = roboto;

		// Upload fonts to Vulkan
		{
			VkCommandPool command_pool = wd->Frames[wd->FrameIndex].CommandPool;
			VkCommandBuffer command_buffer = wd->Frames[wd->FrameIndex].CommandBuffer;

			check_vk_result(
				vkResetCommandPool(this->vk_device, command_pool, 0)
			);

			VkCommandBufferBeginInfo begin_info = {};
			begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
			begin_info.flags |= VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

			check_vk_result(
				vkBeginCommandBuffer(command_buffer, &begin_info)
			);

			ImGui_ImplVulkan_CreateFontsTexture(command_buffer);

			VkSubmitInfo end_info = {};
			end_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
			end_info.commandBufferCount = 1;
			end_info.pCommandBuffers = &command_buffer;
			check_vk_result(
				vkEndCommandBuffer(command_buffer)
			);
			check_vk_result(
				vkQueueSubmit(this->vk_queue, 1, &end_info, VK_NULL_HANDLE)
			);

			check_vk_result(
				vkDeviceWaitIdle(this->vk_device)
			);
			ImGui_ImplVulkan_DestroyFontUploadObjects();
		}
	}

	MainApplication::~MainApplication(void) {
		for(auto &layer: this->layer_stack) {
			layer->on_detach();
		}
		this->layer_stack.clear();

		check_vk_result(vkDeviceWaitIdle(this->vk_device));

		for(auto &queue: this->vk_resource_free_queue) {
			for(auto &func: queue)
				func();
		}
		this->vk_resource_free_queue.clear();

		ImGui_ImplVulkan_Shutdown();
		ImGui_ImplGlfw_Shutdown();
		ImGui::DestroyContext();

		ImGui_ImplVulkanH_DestroyWindow(this->vk_instance, this->vk_device, &this->main_window_data, this->vk_allocator);

		vkDestroyDescriptorPool(this->vk_device, this->vk_descriptor_pool, this->vk_allocator);
		vkDestroyDevice(this->vk_device, this->vk_allocator);
		vkDestroyInstance(this->vk_instance, this->vk_allocator);

		glfwDestroyWindow(this->window_handle);
		glfwTerminate();

		this->running = false;

		this->instance = nullptr;
	}

	void MainApplication::setup_vulkan(const char **extensions, const uint32_t extensions_count) {
		VkResult err;

		// Creating vulkan instance:
		{
			VkInstanceCreateInfo create_info = {};
			create_info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
			create_info.enabledExtensionCount = extensions_count;
			create_info.ppEnabledExtensionNames = extensions;

			// Create Vulkan Instance without any debug feature
			err = vkCreateInstance(&create_info, this->vk_allocator, &this->vk_instance);
			check_vk_result(err);
		}

		// Choosing a GPU:
		{
			uint32_t gpu_count = 0;
			err = vkEnumeratePhysicalDevices(this->vk_instance, &gpu_count, NULL);
			check_vk_result(err);
			IM_ASSERT(gpu_count > 0);

			VkPhysicalDevice *gpus = static_cast<VkPhysicalDevice*>(malloc(sizeof(VkPhysicalDevice) * gpu_count));
			err = vkEnumeratePhysicalDevices(this->vk_instance, &gpu_count, gpus);
			check_vk_result(err);

			for(size_t idx=0; idx < gpu_count; idx++) {
				VkPhysicalDeviceProperties prop;
				vkGetPhysicalDeviceProperties(gpus[idx], &prop);

				if( prop.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU ) {
					this->vk_physical_device = gpus[idx];
				}
			}
			free(gpus);
		}

		// Getting the graphic queue:
		{
			uint32_t queue_count = 0;
			vkGetPhysicalDeviceQueueFamilyProperties(this->vk_physical_device, &queue_count, NULL);
			VkQueueFamilyProperties *queue = static_cast<VkQueueFamilyProperties*>(malloc(sizeof(VkQueueFamilyProperties) * queue_count));
			vkGetPhysicalDeviceQueueFamilyProperties(this->vk_physical_device, &queue_count, queue);
			for(size_t idx=0; idx < queue_count; idx++) {
				if( queue[idx].queueFlags & VK_QUEUE_GRAPHICS_BIT ) {
					this->vk_queue_family = idx;
					break;
				}
			}
			free(queue);
			IM_ASSERT(this->vk_queue_family != (uint32_t)-1);
		}

		// Create a device to work with:
		{
			int device_extension_count = 1;
			const char *device_extensions[] = {"VK_KHR_swapchain"};
			const float queue_priority[] = {1.f};

			VkDeviceQueueCreateInfo queue_info[1] = {};
			queue_info[0].sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
			queue_info[0].queueFamilyIndex = this->vk_queue_family;
			queue_info[0].queueCount = 1;
			queue_info[0].pQueuePriorities = queue_priority;

			VkDeviceCreateInfo create_info = {};
			create_info.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
			create_info.queueCreateInfoCount = sizeof(queue_info) / sizeof(queue_info[0]);
			create_info.pQueueCreateInfos = queue_info;
			create_info.enabledExtensionCount = device_extension_count;
			create_info.ppEnabledExtensionNames = device_extensions;

			err = vkCreateDevice(this->vk_physical_device, &create_info, this->vk_allocator, &this->vk_device);
			check_vk_result(err);
			vkGetDeviceQueue(this->vk_device, this->vk_queue_family, 0, &this->vk_queue);
		}

		// Create descriptor pool:
		{
			VkDescriptorPoolSize pool_sizes[] = {
				{ VK_DESCRIPTOR_TYPE_SAMPLER, 1000 },
				{ VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1000 },
				{ VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, 1000 },
				{ VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 1000 },
				{ VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER, 1000 },
				{ VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER, 1000 },
				{ VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1000 },
				{ VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1000 },
				{ VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC, 1000 },
				{ VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC, 1000 },
				{ VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT, 1000 }
			};

			VkDescriptorPoolCreateInfo pool_info = {};
			pool_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
			pool_info.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
			pool_info.maxSets = 1000 * IM_ARRAYSIZE(pool_sizes);
			pool_info.poolSizeCount = (uint32_t)IM_ARRAYSIZE(pool_sizes);
			pool_info.pPoolSizes = pool_sizes;

			err = vkCreateDescriptorPool(this->vk_device, &pool_info, this->vk_allocator, &this->vk_descriptor_pool);
			check_vk_result(err);
		}
	}

	void MainApplication::setup_vulkan_window(ImGui_ImplVulkanH_Window* wd, VkSurfaceKHR surface, int width, int height) {
		wd->Surface = surface;

		VkBool32 res;
		vkGetPhysicalDeviceSurfaceSupportKHR(this->vk_physical_device, this->vk_queue_family, wd->Surface, &res);
		if( res != VK_TRUE ) {
			throw std::runtime_error("No WSI support for selected device.");
		}

		const VkFormat request_surf_img_format[] = {
			VK_FORMAT_B8G8R8A8_UNORM,
			VK_FORMAT_R8G8B8A8_UNORM,
			VK_FORMAT_B8G8R8_UNORM,
			VK_FORMAT_R8G8B8_UNORM,
		};
		const VkColorSpaceKHR request_surf_color_space = VK_COLORSPACE_SRGB_NONLINEAR_KHR;
		wd->SurfaceFormat = ImGui_ImplVulkanH_SelectSurfaceFormat(
			this->vk_physical_device,
			wd->Surface,
			request_surf_img_format,
			(size_t)IM_ARRAYSIZE(request_surf_img_format),
			request_surf_color_space
		);

		VkPresentModeKHR present_modes[] = { VK_PRESENT_MODE_FIFO_KHR };
		wd->PresentMode = ImGui_ImplVulkanH_SelectPresentMode(
			this->vk_physical_device,
			wd->Surface,
			&present_modes[0],
			IM_ARRAYSIZE(present_modes)
		);

		ImGui_ImplVulkanH_CreateOrResizeWindow(
			this->vk_instance,
			this->vk_physical_device,
			this->vk_device,
			wd,
			this->vk_queue_family,
			this->vk_allocator,
			width,
			height,
			MIN_IMAGE_COUNT
		);
	}

	VkCommandBuffer MainApplication::get_command_buffer([[maybe_unused]]bool begin) {
		ImGui_ImplVulkanH_Window *wd = &this->main_window_data;

		VkCommandPool command_pool = wd->Frames[wd->FrameIndex].CommandPool;

		VkCommandBufferAllocateInfo cmd_buffer_allocate_info = {};
		cmd_buffer_allocate_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
		cmd_buffer_allocate_info.commandPool = command_pool;
		cmd_buffer_allocate_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
		cmd_buffer_allocate_info.commandBufferCount = 1;

		VkCommandBuffer &command_buffer = this->vk_allocated_command_buffers[wd->FrameIndex].emplace_back();
		check_vk_result(
			vkAllocateCommandBuffers(
				this->vk_device,
				&cmd_buffer_allocate_info,
				&command_buffer
			)
		);

		VkCommandBufferBeginInfo begin_info = {};
		begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		begin_info.flags |= VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

		check_vk_result(
			vkBeginCommandBuffer(command_buffer, &begin_info)
		);

		return command_buffer;
	}

	void MainApplication::flush_command_buffer(VkCommandBuffer buffer) {
		const uint64_t DEFAULT_FENCE_TIMEOUT = 100000000000;

		VkSubmitInfo end_info = {};
		end_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		end_info.commandBufferCount = 1;
		end_info.pCommandBuffers = &buffer;
		check_vk_result(vkEndCommandBuffer(buffer));

		VkFenceCreateInfo fence_create_info = {};
		fence_create_info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
		fence_create_info.flags = 0;
		VkFence fence;
		check_vk_result(vkCreateFence(this->vk_device, &fence_create_info, nullptr, &fence));

		check_vk_result(vkQueueSubmit(this->vk_queue, 1, &end_info, fence));

		check_vk_result(vkWaitForFences(this->vk_device, 1, &fence, VK_TRUE, DEFAULT_FENCE_TIMEOUT));

		vkDestroyFence(this->vk_device, fence, nullptr);
	}

	void MainApplication::render_frame(ImDrawData *draw_data) {
		ImGui_ImplVulkanH_Window *wd = &this->main_window_data;
		VkResult err;

		VkSemaphore image_semaphore = wd->FrameSemaphores[wd->SemaphoreIndex].ImageAcquiredSemaphore;
		VkSemaphore render_semaphore = wd->FrameSemaphores[wd->SemaphoreIndex].RenderCompleteSemaphore;

		err = vkAcquireNextImageKHR(this->vk_device, wd->Swapchain, UINT64_MAX, image_semaphore, VK_NULL_HANDLE, &wd->FrameIndex);
		if( err == VK_ERROR_OUT_OF_DATE_KHR || err == VK_SUBOPTIMAL_KHR ) {
			this->vk_swap_chain_rebuild = true;
			return;
		}
		check_vk_result(err);

		this->vk_current_frame_index = (this->vk_current_frame_index + 1) % this->main_window_data.ImageCount;

		ImGui_ImplVulkanH_Frame *fd = &wd->Frames[wd->FrameIndex];
		check_vk_result(
			vkWaitForFences(this->vk_device, 1, &fd->Fence, VK_TRUE, UINT64_MAX)
		);
		check_vk_result(
			vkResetFences(this->vk_device, 1, &fd->Fence)
		);

		for(auto &f: this->vk_resource_free_queue[this->vk_current_frame_index])
			f();
		this->vk_resource_free_queue[this->vk_current_frame_index].clear();

		{
			auto &allocated_cmd_buffers = this->vk_allocated_command_buffers[wd->FrameIndex];
			if( allocated_cmd_buffers.size() > 0 ) {
				vkFreeCommandBuffers(
					this->vk_device,
					fd->CommandPool,
					(uint32_t)allocated_cmd_buffers.size(),
					allocated_cmd_buffers.data()
				);
				allocated_cmd_buffers.clear();
			}

			check_vk_result(
				vkResetCommandPool(this->vk_device, fd->CommandPool, 0)
			);

			VkCommandBufferBeginInfo info = {};
			info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
			info.flags |= VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
			check_vk_result(
				vkBeginCommandBuffer(fd->CommandBuffer, &info)
			);
		}

		{
			VkRenderPassBeginInfo info = {};
			info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
			info.renderPass = wd->RenderPass;
			info.framebuffer = fd->Framebuffer;
			info.renderArea.extent.width = wd->Width;
			info.renderArea.extent.height = wd->Height;
			info.clearValueCount = 1;
			info.pClearValues = &wd->ClearValue;
			vkCmdBeginRenderPass(fd->CommandBuffer, &info, VK_SUBPASS_CONTENTS_INLINE);
		}

		ImGui_ImplVulkan_RenderDrawData(draw_data, fd->CommandBuffer);
		vkCmdEndRenderPass(fd->CommandBuffer);

		{
			VkPipelineStageFlags wait_stage = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
			VkSubmitInfo info = {};
			info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
			info.waitSemaphoreCount = 1;
			info.pWaitSemaphores = &image_semaphore;
			info.pWaitDstStageMask = &wait_stage;
			info.commandBufferCount = 1;
			info.pCommandBuffers = &fd->CommandBuffer;
			info.signalSemaphoreCount = 1;
			info.pSignalSemaphores = &render_semaphore;

			check_vk_result(
				vkEndCommandBuffer(fd->CommandBuffer)
			);
			check_vk_result(
				vkQueueSubmit(this->vk_queue, 1, &info, fd->Fence)
			);
		}
	}

	void MainApplication::present_frame(void) {
		if( this->vk_swap_chain_rebuild )
			return ;

		ImGui_ImplVulkanH_Window *wd = &this->main_window_data;
		VkSemaphore render_semaphore = wd->FrameSemaphores[wd->SemaphoreIndex].RenderCompleteSemaphore;
		VkPresentInfoKHR info = {};
		info.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
		info.waitSemaphoreCount = 1;
		info.pWaitSemaphores = &render_semaphore;
		info.swapchainCount = 1;
		info.pSwapchains = &wd->Swapchain;
		info.pImageIndices = &wd->FrameIndex;

		VkResult err = vkQueuePresentKHR(this->vk_queue, &info);
		if( err == VK_ERROR_OUT_OF_DATE_KHR || err == VK_SUBOPTIMAL_KHR ) {
			this->vk_swap_chain_rebuild = true;
			return;
		}
		check_vk_result(err);

		wd->SemaphoreIndex = (wd->SemaphoreIndex + 1) % wd->ImageCount;
	}

	float MainApplication::get_time(void) {
		return (float)glfwGetTime();
	}

	void MainApplication::run(void) {
		this->running = true;

		ImGui_ImplVulkanH_Window *wd = &this->main_window_data;
		ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);
		ImGuiIO &io = ImGui::GetIO();

		while( !glfwWindowShouldClose(this->window_handle) && this->running ) {
			glfwPollEvents();

			for(auto &layer : this->layer_stack)
				layer->on_update(this->time_step);

			if( this->vk_swap_chain_rebuild ) {
				int width, height;

				glfwGetFramebufferSize(this->window_handle, &width, &height);
				if( width > 0 && height > 0 ) {
					ImGui_ImplVulkan_SetMinImageCount(MIN_IMAGE_COUNT);
					ImGui_ImplVulkanH_CreateOrResizeWindow(
						this->vk_instance,
						this->vk_physical_device,
						this->vk_device,
						&this->main_window_data,
						this->vk_queue_family,
						this->vk_allocator,
						width,
						height,
						MIN_IMAGE_COUNT
					);
					this->main_window_data.FrameIndex = 0;

					this->vk_allocated_command_buffers.clear();
					this->vk_allocated_command_buffers.resize(this->main_window_data.ImageCount);

					this->vk_swap_chain_rebuild = false;
				}
			}

			ImGui_ImplVulkan_NewFrame();
			ImGui_ImplGlfw_NewFrame();
			ImGui::NewFrame();

			// ImGui rendering:
			{
				static ImGuiDockNodeFlags dockspace_flags = ImGuiDockNodeFlags_None;

				ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoDocking;
				if( this->menubar_callback )
					window_flags |= ImGuiWindowFlags_MenuBar;

				const ImGuiViewport* viewport = ImGui::GetMainViewport();
				ImGui::SetNextWindowPos(viewport->WorkPos);
				ImGui::SetNextWindowSize(viewport->WorkSize);
				ImGui::SetNextWindowViewport(viewport->ID);
				ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
				ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
				window_flags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove;
				window_flags |= ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;

				if( dockspace_flags & ImGuiDockNodeFlags_PassthruCentralNode )
					window_flags |= ImGuiWindowFlags_NoBackground;

				ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
				ImGui::Begin("Dock", nullptr, window_flags);
				ImGui::PopStyleVar();

				ImGui::PopStyleVar(2);

				ImGuiIO& io = ImGui::GetIO();
				if( io.ConfigFlags & ImGuiConfigFlags_DockingEnable ) {
					ImGuiID dockspace_id = ImGui::GetID("VulkanAppDockspace");
					ImGui::DockSpace(dockspace_id, ImVec2(0.0f, 0.0f), dockspace_flags);
				}

				if( this->menubar_callback ) {
					if (ImGui::BeginMenuBar()) {
						this->menubar_callback();
						ImGui::EndMenuBar();
					}
				}

				for (auto& layer : this->layer_stack)
					layer->on_gui_rendering();

				ImGui::End();
			}

			ImGui::Render();
			ImDrawData *main_draw_data = ImGui::GetDrawData();
			const bool main_is_minimised = (main_draw_data->DisplaySize.x <= 0.f || main_draw_data->DisplaySize.y <= 0.f);

			wd->ClearValue.color.float32[0] = clear_color.x * clear_color.w;
			wd->ClearValue.color.float32[1] = clear_color.y * clear_color.w;
			wd->ClearValue.color.float32[2] = clear_color.z * clear_color.w;
			wd->ClearValue.color.float32[3] = clear_color.w;
			if( !main_is_minimised )
				this->render_frame(main_draw_data);

			if( io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable ) {
				ImGui::UpdatePlatformWindows();
				ImGui::RenderPlatformWindowsDefault();
			}

			if( !main_is_minimised )
				this->present_frame();

			float time = this->get_time();
			this->frame_time = time - this->last_frame_time;
			this->time_step = glm::min<float>(this->frame_time, 0.0333f);
			this->last_frame_time = time;
		}
	}
} // namespace vugui
