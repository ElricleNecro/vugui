#include "vugui/random.h"

namespace vugui {
	thread_local std::mt19937 RandomNumber::random_engine;
	std::uniform_int_distribution<std::mt19937::result_type> RandomNumber::distribution;
} // namespace vugui
